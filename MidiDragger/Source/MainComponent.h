/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

//class MidiDragComponent   : public Component, public DragAndDropContainer
//{
//public:
//
//};

//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component, public FileDragAndDropTarget, public Button::Listener
{
public:
    //==============================================================================
    MainComponent();
    ~MainComponent();
    
    
    class Pattern
    {
    public:
        int index = -1;
        int Uuid = 0;
        int NumOfNotes = 16;
        Array<int> patternNote;
        Array<float> patternVelocity;
        Array<int> patternRoll;
        int ppq = 240;
   };
    
    class Track
    {
    public:
        int index = -1;
        int nudgeAmount = 0;
        ScopedPointer<Pattern> pattern;
        
    };
    
    //==============================================================================
    void paint (Graphics&) override;
    void resized() override;
    void buttonClicked (Button*) override;

private:
    //FileDragAndDropTarget
    void filesDropped (const StringArray& files, int x, int y) override;
    bool isInterestedInFileDrag (const StringArray& files) override;
    void fileDragEnter (const StringArray& files, int x, int y) override;
    void fileDragExit (const StringArray& files) override;
    
    //==============================================================================
    // Your private member variables go here...
    ScopedPointer<TextButton> midiButton;
    OwnedArray<Button> textButtonArray;
    String mainLocationPath;
    bool fileDetected = false;
    void interplateJsonFile();
    void readTrack (var tracksVar);
    void readPattern (var patternVar);
    void createMidiFile();
    var inputVar;
    OwnedArray<Track> tracksInfo;
    double swingAmount = 100;
   


    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};
