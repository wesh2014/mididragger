/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"

//==============================================================================
MainComponent::MainComponent()
{
    for (int i = 0; i < 8; i++)
    {
        TextButton* button;
        addAndMakeVisible (button = new TextButton ("Midi - " + String (i+1), "midiDrag"));
        button->setComponentID (String (i));
        button->addListener (this);
        button->setTriggeredOnMouseDown (true);
        textButtonArray.add (button);
    }
    mainLocationPath = File::getSpecialLocation (File::SpecialLocationType::userApplicationDataDirectory).getFullPathName()
                     + File::getSeparatorString() + "TechNiKali" + File::getSeparatorString() + ProjectInfo::projectName + File::getSeparatorString();
   setSize (400, 400);
}

MainComponent::~MainComponent()
{
    File file (mainLocationPath);
    file.deleteRecursively();
}

//------------------------------------------------------------------------------
void MainComponent::filesDropped (const StringArray& files, int x, int y)
{
    File droppedFile (files[0]);
    
    if (droppedFile.existsAsFile())
    {
        inputVar = JSON::parse (droppedFile);
        interplateJsonFile();
    }
}

//------------------------------------------------------------------------------
bool MainComponent::isInterestedInFileDrag (const StringArray& files)
{
    File draggedFile (files[0]);
 
    if (draggedFile.getFileExtension() == ".lcdp")
        return true;
    else
        return false;
}

//------------------------------------------------------------------------------
void MainComponent::fileDragEnter (const StringArray& files, int x, int y)
{
    fileDetected = true;
    repaint();
}

//------------------------------------------------------------------------------
void MainComponent::fileDragExit (const StringArray& files)
{
    fileDetected = false;
    repaint();
}

//------------------------------------------------------------------------------
void MainComponent::interplateJsonFile()
{
   if (! inputVar.hasProperty ("tracks"))
   {
       jassertfalse;
       return;
   }
    DynamicObject* mainObject = inputVar.getDynamicObject();
    swingAmount = mainObject->getProperty("swing");
    var trackVar = mainObject->getProperty ("tracks");
    if (! trackVar.isArray())
    {
        jassertfalse;
        return;
    }
    int numberOfTracks = trackVar.size();
    for (int i = 0; i < numberOfTracks; i++)
    {
        readTrack( trackVar[i]);
    }
    
    var patternVar = mainObject->getProperty ("patterns");
    if (! patternVar.isArray())
    {
        jassertfalse;
        return;
    }
    int numberOfPatterns = patternVar.size();
    for (int i = 0; i < numberOfPatterns; i++)
    {
        readPattern (patternVar[i]);
    }
}

//------------------------------------------------------------------------------
void MainComponent::readTrack (var tracksVar)
{
    DynamicObject* trackObject = tracksVar.getDynamicObject();
    Track* track = new Track();
    track->index = trackObject->getProperty ("index");
    track->nudgeAmount = trackObject->getProperty("nudgetrack");
    
    tracksInfo.add (track);
}

//------------------------------------------------------------------------------
void MainComponent::readPattern (var patternVar)
{
   DynamicObject* patternObject = patternVar.getDynamicObject();
    for (int i = 0; i < tracksInfo.size(); i++)
    {
        Track* track = tracksInfo.getUnchecked (i);
        if (track == nullptr)
        {
            jassertfalse;
            return;
        }
        Pattern* pattern = new Pattern();
        pattern->Uuid = patternObject->getProperty("Uuid");
        pattern->index = patternObject->getProperty("index");
        var patternNoteVar  = patternObject->getProperty ("pattern_Note");
        int numberOfNotes = 16;
        int start = track->index * 17;
        int end = numberOfNotes + start;
        for (int j = start; j < end; j++)
        {
            pattern->patternNote.add (patternNoteVar[j]);
        }
        
        var patternRollVar  = patternObject->getProperty ("pattern_Roll");
        for (int j = start; j < end; j++)
        {
            int roll = patternRollVar[j];
            pattern->patternRoll.add (roll);
        }
        
        
        var patternVelVar  = patternObject->getProperty ("pattern_Vel");
        for (int j = start; j < end; j++)
        {
            float tempVel = patternVelVar[j];
            float value = jmap (tempVel, 0.0f, 127.0f, 0.0f, 1.0f);
            pattern->patternVelocity.add (value);
        }
        
        track->pattern = pattern;
    }
    createMidiFile();
}

//------------------------------------------------------------------------------
void MainComponent::createMidiFile()
{
    MidiFile midiFile;
    int index = -1;
   ScopedPointer<MidiMessageSequence> midiSeq = new MidiMessageSequence();
    for (int i = 0; i < tracksInfo.size(); i++)
    {
        Track* track = tracksInfo.getUnchecked (i);
        if (track == nullptr)
            jassertfalse;
        
        Pattern* pattern = track->pattern;
        index = pattern->Uuid;
        midiFile.setTicksPerQuarterNote (pattern->ppq);
        double eventLength = pattern->ppq / 4;
        double nudgeTime = track->nudgeAmount / 2;
        for (int j = 0; j < pattern->NumOfNotes; j++ )
        {
            double time = (eventLength * j) + nudgeTime;
            double nextTime = (eventLength * (j + 1)) + nudgeTime;
            double swingTime = (nextTime - time) * (swingAmount / 100);
            int temp = j + 1;
            if ((temp % 2) == 0 && j > 0)
            {
                time += swingTime;
            }
            
            if (pattern->patternNote[j])
            {
                int numOfRolls = pattern->patternRoll[j];
                double noteLength = eventLength / numOfRolls;
                
                for (int roll = 0 ; roll < numOfRolls ; roll++)
                {
                    MidiMessage messageOn = MidiMessage::noteOn(1, i + 36, pattern->patternVelocity[j]);
                    double tempTime = time + (noteLength * roll);
                    messageOn.setTimeStamp (tempTime);
                    midiSeq->addEvent (messageOn);
                    
                    tempTime = tempTime + noteLength;
                    MidiMessage messageOff = MidiMessage::noteOff(1, i + 36, 0.0f);
                    messageOff.setTimeStamp (tempTime);
                    midiSeq->addEvent (messageOff);
                }
              }
         }
       }
    midiFile.addTrack (*midiSeq);
    File outputFile (mainLocationPath + File::getSeparatorString() + "midi-" + String (index) + ".mid");
    textButtonArray[index - 1]->setColour(TextButton::ColourIds::buttonColourId, Colours::green.withAlpha(0.25f));
    if (outputFile.existsAsFile())
        outputFile.deleteFile();
    
    outputFile.create();
  ScopedPointer<OutputStream> outputStream = outputFile.createOutputStream();
    midiFile.writeTo (*outputStream);
}

//==============================================================================
void MainComponent::paint (Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));

   
    if (fileDetected)
    {
         g.setFont (Font (20.0f));
        g.setColour (Colours::green.withAlpha(0.5f));
        g.drawText ("Drop File!", getLocalBounds(), Justification::centred, true);
    }
    else
    {
        g.setFont (Font (16.0f));
        g.setColour (Colours::white);
        g.drawText ("Drag File Here", getLocalBounds(), Justification::centred, true);
    }
   
}

void MainComponent::resized()
{
    // This is called when the MainComponent is resized.
    // If you add any child components, this is where you should
    // update their positions.
    int xPos = getWidth() - 100;
    for (int i = 0; i < 8; i++ )
    {
        Rectangle<int> rect(xPos, i * 50, 100, 50);
        textButtonArray[i]->setBounds(rect.reduced (2));
    }
   
}

void MainComponent::buttonClicked (Button* button)
{
    int index = textButtonArray.indexOf (button) + 1;
    File outputFile (mainLocationPath + File::getSeparatorString() + "midi-"+ String (index) + ".mid");
    DragAndDropContainer::performExternalDragDropOfFiles(outputFile.getFullPathName(), true );
}
